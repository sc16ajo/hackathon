// player.java
// Ashley Oldershaw October 2017
// The class for a character

public class Player {
  protected String name;
  protected String job;
  protected double salary;

  protected int age;

  public Player (String initialName, String initialJob, double initialSalary) {
    name = initialName;
    job = initialJob;
    salary = initialSalary;
  }

  public String getName() { return name; }
  public String getJob() { return job; }
  public double getSalary() { return salary; }

  public void setName(String newName)     { name = newName; }
  public void setJob(String newJob)       { job = newJob; }
  public void setSalary(double newSalary) { salary = newSalary; }
  public void birthday ()             { age += 1; }
}
