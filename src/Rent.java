// Rent.java
// Ashley Oldershaw October 2017
// A hole for money that you spend on rent

public class Rent extends BankAccount {

  public Rent (Player newPlayer) {
    super(newPlayer, 0);
    quarterlyPayment = 1500;
  }

  public boolean withdraw(BankAccount recipient, double withdrawValue) { return false; }
}
