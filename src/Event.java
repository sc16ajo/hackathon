// Event.java
// Ashley Oldershaw October 2017
// This is to simulate news events

package com.concentratedquaggajuice.investment;

public class Event {
  protected int id;
  protected String title;
  protected String description;
  protected int occurences;

  //Stock and multiplier. Multiplier should be between -1 an 1
  protected Map<Integer, Double> effects = new HashMap<>();

  public Event (int newID,  String newTitle, String newDescription, int newOccurences, Map<Stock, Double> newEffects) {
    id = newID;
    title = newTitle;
    description = newDescription;
    occurences = newOccurences;
    effects = newEffects;
  }

  public void doEvent(){

    for(Map.Entry<Integer, Double> effect : effects.entrySet()) {

      Stock key = entry.getKey();
      double multi = entry.getValue();


      //For each effect that happens apply the event to the stock
      key.applyEvent(multi);
    }

  }

  public int getID ()       { return id; }
  public String getTitle () { return title; }
  public String getText  () { return text; }
  public void decreaseOccurences () { occurences -= 1; }
}