// Stock.java
// Ashley Oldershaw October 2017
import java.Math.*;

public class Stock {

  static Map<Integer, Double> avaliableStocks = new HashMap<>();

  protected  int id;

  protected String name;
  protected double price;

  protected int risk;

  public String getName () { return name; }
  public double getPrice () { return price; }
  public void   setPrice (double newPrice) { price = newPrice; }

  public Stock (int newId, string newName, double startPrice, int newRisk ){

      id = newId;
      name = newName;
      setPrice(startPrice);
      risk = newRisk;

      avaliableStocks.put(id, );

  }

  //Note: if the id is invalid this will return null
  public static Stock getStockById(int stockID){
      return avaliableStocks.get(stockID);
  }

  public void applyRisk(){

    double percentage = 0;
    double randMult = Math.random();

    if (risk == 0){
      //Very Low -1 to +3
      percentage = (randMult * 4) - 1;

    } else if (risk == 1){
      //Low -3 to +6
      percentage = (randMult * 9) - 3;

    } else if (risk == 2){
      //Medium -8 to +14
      percentage = (randMult * 22) - 8;

    } else if (risk == 3){
      //High -45 to + 55
      percentage = (randMult * 100) - 45;

    } else {
      //Something bad happened
      percentage = 0;

    }

    //Apply this percentage
    this.setPrice(price * ((percentage / 100) + 1);

  }

  public void applyEvent(double multiPl){
      this.setPrice(price * multiPl);
  }
}
