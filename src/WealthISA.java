// WealthISA.java
// Ashley Oldershaw October 2017
// The investment account, generates constant good returns

import java.util.Random;

public class WealthISA extends BankAccount {

  protected double charges;

  public WealthISA (Player newPlayer, double initialBalance) {
    super (newPlayer, initialBalance);
  }

  // apply randomly generated interest
  @Override
  public void applyInterest () {
    Random generator = new Random();

    double initial = generator.nextDouble();
    rate = 1 + ((initial * 5.0) / 100);
    balance *= rate;
    return;
  }

  // apply charges
  public void applyCharges () {
    balance *= charges;
    return;
  }

  public double getCharges (double newCharges) { return charges; }

  public void setCharges (double newCharges) { charges = newCharges; }
}
