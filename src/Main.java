// Main.java
// Ashley Oldershaw October 2017
// The prototype game

public class Main {

  public static Deductions deductions;
  public static Rent rent;
  public static CashISA cashISA;
  public static WealthISA wealthISA;
  public static PensionAccount pension;
  public static CurrentAccount current;
  public static Mortgage mortgage;
  public static Player player;
  public static int turn;

  // go onto the next turn, increment the quarter and apply interest if it's a new year
  public static void nextTurn () {

    // get paid, then pay rent/mortgage and add monthly payments
    current.deposit (player.getSalary() / 4.0);
    current.withdraw(mortgage, mortgage.getQuarterlyPayment());
    current.withdraw(cashISA,  cashISA.getQuarterlyPayment());
    current.withdraw(pension,  pension.getQuarterlyPayment());

    current.withdraw(rent,        rent.getQuarterlyPayment());
    current.withdraw(deductions,  deductions.getQuarterlyPayment());
    turn += 1;

    // apply interest to account
    cashISA.applyInterest();
    pension.applyInterest();
    mortgage.applyInterest();
    wealthISA.applyInterest();

    // increment age and ISA allowance every year, get promoted
    if (turn % 4 == 0) {
      cashISA.addAllowance(20000);
      player.birthday();
      player.setSalary (player.getSalary() * 1.04);
    }
  }

  public static void printBalances() {
    System.out.printf ("\nTurn: %d\n", turn);
    System.out.printf ("CURRENT ACCOUNT BALANCE: %f\n", current.getBalance());
    System.out.printf ("SAVINGS ACCOUNT BALANCE: %f\n", cashISA.getBalance());
    System.out.printf ("PENSION ACCOUNT BALANCE: %f\n", pension.getBalance());
    System.out.printf ("WEALTH ACCOUNT BALANCE: %f\n", wealthISA.getBalance());
    System.out.printf ("MORTGAGE TO REPAY BACK: %f\n", -mortgage.getBalance());
  }

  public static void main(String[] args) {

    turn = 0;

    // initialise player
    player   = new Player ("Quagga", "Hacker", 20000);
    // start current account
    current  = new CurrentAccount (player, 400);
    // savings accounts
    cashISA  = new CashISA (player, 100, 1.002, 20000);
    cashISA.setQuarterlyPayment(500);
    pension  = new PensionAccount (player, 0, 1.01);
    pension.setQuarterlyPayment(500);

    wealthISA = new WealthISA (player, 0);
    current.withdraw(wealthISA, 1000);
    wealthISA.setQuarterlyPayment(500);

    rent = new Rent (player);
    deductions = new Deductions (player);

    mortgage = new Mortgage (player, -100000, 1.0037);

    System.out.printf (" GAME INITIALISED \n");

    for (int i = 0; i < 10 ; i++) {
      printBalances();
      nextTurn();
    }


    printBalances();
  }
}
