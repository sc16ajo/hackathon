// SavingsAccount.java
// Ashley Oldershaw October 2017
// The savings account, it's used for medium term saving

import java.math.*;

public class SavingsAccount extends BankAccount {

  public SavingsAccount (Player newPlayer, double initialBalance, double initialRate) {
    super(newPlayer, initialBalance);
    rate = initialRate;
  }

  @Override
  // withdraw money from bank account, takes from balance and returns true if
  // positive, returns false if negative
  public boolean withdraw(BankAccount recipient, double withdrawValue) {
    if ((balance - withdrawValue < 0)) {
      balance -= withdrawValue;
      return true;
    }
    else
      return false;
  }



}
