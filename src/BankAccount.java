// BankAccount.java
// Ashley Oldershaw October 2017
// The basic bank account

public class BankAccount {

  protected double balance;
  protected Player player;
  protected double rate;
  protected double monthlyPayment;
  protected double quarterlyPayment;

  public BankAccount (Player newPlayer, double initialBalance) {
    player = newPlayer;
    balance = initialBalance;
  }

  // deposit money into bank account, adds to balance and returns true if
  // positive, returns false if negative
  public boolean deposit(double deposit) {
    if (deposit > 0) {
      balance += deposit;
      return true;
    }
    else
      return false;
  }

  // withdraw money from bank account, takes from balance and returns true if
  // positive, returns false if negative
  public boolean withdraw(BankAccount recipient, double withdrawValue) {
    if (withdrawValue > 0) {
      if (recipient.deposit(withdrawValue) == true) {
        balance -= withdrawValue;
        return true;
      }
      else return false;
    }
    else
      return false;
  }

  // apply interest
  public void applyInterest () {
    balance *= rate;
    return;
  }

  // getters

  // return name
  public Player getPlayer() {return player;}

  // return balance
  public double getBalance() {return balance;}

  // return rate
  public double getRate() {return rate;}

  // return quarterly payment
  public double getQuarterlyPayment() {return quarterlyPayment;}

  // setters

  // set rate. name doesn't change and you can't just set balance
  public void setRate (float newRate) {rate = newRate;}

  public void setQuarterlyPayment(double newQuarterly) { quarterlyPayment = newQuarterly; }
}
