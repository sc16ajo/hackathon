// Deductions.java
// Ashley Oldershaw October 2017
// A hole for money that you spend on living expenses / cars / weddings

public class Deductions extends BankAccount {

  public Deductions (Player newPlayer) {
    super(newPlayer, 0);
    quarterlyPayment = 1500;
  }

  public boolean withdraw(BankAccount recipient, double withdrawValue) { return false; }
}
