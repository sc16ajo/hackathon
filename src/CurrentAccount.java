// CurrentAccount.java
// Ashley Oldershaw October 2017
// The basic current account. no interest or anything, used to allocate money to day to day things and mortgage

public class CurrentAccount extends BankAccount {

  public CurrentAccount (Player newPlayer, double initialBalance) {
    super(newPlayer, initialBalance);
  }

  // deposit money into bank account, adds to balance and returns true if
  // positive, returns false if negative
  @Override
  public boolean deposit(double deposit) {
    if (deposit > 0 && (deposit + balance) > 1000.0) {
      balance += deposit;
      return true;
    }
    else
      return false;
  }
}
